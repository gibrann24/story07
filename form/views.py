from django.shortcuts import render
from django.http import HttpResponseRedirect

from .forms import StatusForm
from .models import StatusFormModels

# Create your views here.

def index(request): #Showing all the status 
    posts = StatusFormModels.objects.all()

    context = {
        'page_title':'Status List',
        'posts':posts,
    }
    return render(request,'form/index.html',context)

    #context = {
        #'heading' : 'Status Forms',
        #'form':'stausForm',
    #}
    #return render(request,'form/index.html',context)

def create(request):#Creating status
    #status_form = StatusForm()
    if request.method == 'POST':
        create_status = StatusFormModels.objects.create(
            name    = request.POST['name'],
            status  = request.POST['status'],
        )
        create_status.save()
        return HttpResponseRedirect("/")
    else:
        return HttpResponseRedirect("/create/")
# Function for confirm
def confirm(request):
    if request.method == "POST":
        response = {
            'page_title' : 'Create Post',
            'name' : request.POST['name'],
            'status' : request.POST['status']
        }
        return render(request, 'form/create.html', response)
    else:
        return HttpResponseRedirect("/form/")

def form(request):
    status_form = StatusForm()
    response = {
        'statusForm' : status_form
    }
    return render(request, 'form/form.html', response)
    
